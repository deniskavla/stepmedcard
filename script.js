//отрисовка интерфейса
let card = document.getElementById('card');
let header = document.createElement('div');
header.className = "header";
let header_logo = document.createElement('div');
header_logo.className = "header_logo";
header_logo.innerHTML = '<img src="./img/logo.png" alt="Медицинская клиника «Добробут»">';
header.append(header_logo);
card.append(header);
let header_create = document.createElement('div');
header_create.className = "header_create";
header_create.setAttribute("data-modal", "modal");
header_create.innerHTML = '<button class="header_create_button">записаться к доктору</button>';
header.append(header_create);
let items = document.createElement('div');
items.className = "items";
card.append(items);
let noItemsText = document.createElement('div');
noItemsText.className = "noItemsText";
noItemsText.innerHTML += 'No items have been added';
items.append(noItemsText);
let token = null;

//Проверка token и как то нужно воспроизвести карты из базы
//записать еще в базу надо прежде чем вытаскивать
document.addEventListener('DOMContentLoaded', async () => {
    await login(account)
    token = sessionStorage.getItem('token');
    List_Cards.downloadVisit(token);
});
///POST запрос
function creatVisit(data) {
    return fetch("http://cards.danit.com.ua/cards", {
        method: "POST",
        headers: {
            "Content-Type": "application/json;charset=utf-8",
            "Authorization": `Bearer ${token}`
        },
        body: JSON.stringify(data)
    }).then(response => response.json())
        .then(result => result)
}
/////////////////  account
const account = {
    email: 'deniska.vla@gmail.com',
    password: "654321aA"
// tokken   75cef965a4aa
};
async function login(account) {
    return fetch("http://cards.danit.com.ua/login", {
        method: "POST",
        headers: {
            "Content-Type": "application/json;charset=utf-8"
        },
        body: JSON.stringify(account)
    }).then(message => message.json())
        .then(message => {
            let token = message.token;
            return token;
        })
        .then(token => sessionStorage.setItem('token', token))
}


//по select создается экземпляр кдасса
class renderDoc { //селект
    constructor(modal) {

    }

    static doctorChoice(modal) {//выбор врача
        ///////////////select врачей
        let select = document.createElement('select');
        select.name = 'doctor';
        select.setAttribute('nameDoc', 'name');
        select.innerHTML += `<option disabled selected>- - - -</option>
                            <option value="Терапевт">Терапевт</option>
                            <option value="Стоматолог">Стоматолог</option>
                            <option value="Кардиолог">Кардиолог</option>`;
        modal.prepend(select);
        ///////////////дополнительные инпуты для докторов
        let additionalInputDoctor = document.createElement('div');
        additionalInputDoctor.className = 'additionalInputDoctor';
        modal.append(additionalInputDoctor);
        select.addEventListener('change', function () {

            let selectDoc = modal.querySelectorAll("select");
            if (selectDoc[0].value === "Терапевт") {
                Therapist.renderTherapist(additionalInputDoctor);
                new Therapist();
            } else if (selectDoc[0].value === "Стоматолог") {
                Dentist.renderDentist(additionalInputDoctor);
                new Dentist();
            } else if (selectDoc[0].value === "Кардиолог") {
                Cardiologist.renderCardiologist(additionalInputDoctor);
                new Cardiologist();
            }
            List_Cards.creatCard(modal);
        });
    };
}

class Visit {
    constructor(element) {
        this.element = element;
    }

    static render() {
        let modal = document.createElement('form');
        modal.className = "modal";
        modal.setAttribute("data-modal", "modal");
        card.append(modal);
        let modalContent = document.createElement('div');
        modalContent.className = "modalContent";
        modalContent.innerHTML = '<svg class="modal__cross" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">' +
            '<path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/>' +
            '</svg>';
        modal.append(modalContent);
        /////////////overlay
        let overlay = document.createElement('div');
        overlay.className = "overlay";
        card.append(overlay);
        document.querySelector('.header_create_button').addEventListener('click', function () {
            modal.style.display = 'block';
            overlay.style.display = 'block';
        });
        document.querySelector('.overlay').addEventListener('click', function () {
            modal.style.display = 'none';
            overlay.style.display = 'none';
        });
        modalContent.firstElementChild.addEventListener('click', function () {
            modal.style.display = 'none';
            overlay.style.display = 'none';
        });
        ///////////////ФИО
        let input_blocks_name = document.createElement('input');
        input_blocks_name.setAttribute('name', 'name');
        input_blocks_name.setAttribute('required', 'required');
        input_blocks_name.setAttribute('id', 'namePatient');
        input_blocks_name.setAttribute('placeholder', 'ФИО');
        modalContent.append(input_blocks_name);
        ///////////////Цель визита
        let input_blocks_purpose = document.createElement('input');
        input_blocks_purpose.name = 'purpose';//Цель визита
        input_blocks_purpose.setAttribute('required', 'required');
        input_blocks_purpose.setAttribute('id', 'purpose');
        input_blocks_purpose.setAttribute('placeholder', 'Цель визита');
        modalContent.append(input_blocks_purpose);
        ///////////////Дата визита
        let input_blocks_date = document.createElement('input');
        input_blocks_date.name = 'visitDate';//Дата визита
        input_blocks_date.setAttribute('required', 'required');
        input_blocks_date.setAttribute('type', 'date');
        input_blocks_date.setAttribute('id', 'visitDate');
        input_blocks_date.setAttribute('placeholder', 'Дата посещения');
        modalContent.append(input_blocks_date);
        ///////////////Коментарии
        let input_blocks_comment = document.createElement('textarea');
        input_blocks_comment.name = 'comment';//
        input_blocks_comment.setAttribute('id', 'comment');
        input_blocks_comment.setAttribute('placeholder', 'Комментарий к визиту');
        input_blocks_comment.setAttribute('maxlength', '400');
        modalContent.append(input_blocks_comment);

        renderDoc.doctorChoice(modal);

    }
}

/////////создания кнопки и сохранение данных на сервере для создания карточки
class List_Cards {
    constructor(modalContent) {
        this.modal = modalContent;
    }

    static creatCard(modal) {
        let creat = document.getElementsByClassName('creating_card_doctor');
        let creating_card_doctor = document.createElement('div');
        creating_card_doctor.className = "creating_card_doctor";
        if (!creat.length) {
            ///////////////Кнопка создания карточки
            creating_card_doctor.innerHTML = '<button type="submit" class="creating_card_doctor">создать запись к' +
                ' доктору</button>';
            modal.append(creating_card_doctor);
        }
        ///////////////Закрытие модалки
        // creating_card_doctor.addEventListener('click', function (e) {
        modal.onsubmit = (e) => {
            e.preventDefault();
            const data = new FormData(modal);

            const formData = {};
            [...data.entries()].forEach(v => {
                formData[v[0]] = v[1];
            });
            // console.log(data.entries());
            // console.log('моя модалка');
            // console.log(formData); ///моя константа объекта

            creatVisit(formData).then((result) => {
                // console.log('ответ от сервака');
                // console.log(result); // ответ от сервера
                this.id = result.id;
                formData.id = this.id;
            });
            modal.style.display = 'none';
            let overlay = document.getElementsByClassName('overlay');
            overlay[0].style.display = 'none';
            List_Cards.renderCards(formData);
        };
    }

    static renderCards(formData) {
        //////создание карточки    на экране
        let medCard = document.createElement('div');
        medCard.className = "medCard";
        medCard.setAttribute('draggable', true);
        items.append(medCard);

        let closeBtn = document.createElement('div');
        closeBtn.className = "closeBtn";
        closeBtn.innerHTML = '<svg class="modal__cross" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 24 24">' +
            '<path d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/>' +
            '</svg>';
        medCard.append(closeBtn);

        ////////drag&Drop
        let elementX;
        let elementY;
        medCard.onmousedown = function (event) {
            elementX = event.offsetX;
            elementY = event.offsetY;
        };
        medCard.addEventListener('dragend', event => {
            event.target.closest('.medCard').style.position = 'absolute'

            medCard.style.display = 'none';
            event.target.style.top = (event.clientY - 75 - elementY) + 'px';
            event.target.style.left = (event.clientX - elementX) + 'px';
            event.target.style.zIndex = 1;
            medCard.style.display = 'block';
        });

        let doctor = document.createElement('div');
        doctor.className = "doctor";
        doctor.innerHTML += 'Запись к ' + formData.doctor;
        medCard.append(doctor);

        let patient = document.createElement('div');
        patient.className = "patient";
        patient.innerHTML = 'Пациент ' + formData.name;
        medCard.append(patient);

        let dataVisit = document.createElement('div');
        dataVisit.className = "patient";
        dataVisit.innerHTML = 'На дату ' + formData.visitDate;
        medCard.append(dataVisit);
        medCard.id = formData.id;
        //////создание кнопки подробнее в карточке
        let moreDetails = document.createElement('div');
        moreDetails.innerHTML = `<a class="moreDetails" href="#">Показать больше</a>`;
        let commit = document.createElement('div');
        commit.className = "commit";
        commit.innerHTML = formData.comment;
        commit.style.display = 'none';
        //создание экземляров соответствущих классов в зависимости от значения в select
        let age_ter = document.createElement('div');
        let dentist = document.createElement('div');
        let cardiol = document.createElement('div');
        if (formData.doctor === 'Терапевт') {
            age_ter.innerHTML = formData.age;
            age_ter.style.display = 'none';
            medCard.append(age_ter);
        } else if (formData.doctor === 'Стоматолог') {
            dentist.innerHTML = formData.last_visit;
            dentist.style.display = 'none';
            medCard.append(dentist);

        } else if (formData.doctor === 'Кардиолог') {
            cardiol.innerHTML = `
                                   <p>${formData.pressure}</p>
                                   <p>${formData.mass_index}</p>
                                   <p>${formData.tolerable_diseases}</p>
                                   <p>${formData.age}</p>
                                 `;

            cardiol.style.display = 'none';
            medCard.append(cardiol);
        }

        //скрыть показать доп инфу по поводу записи к доктору
        moreDetails.addEventListener('click', function () {
            medCard.classList.toggle("details");
            //////костыльно как-то ((((
            if (medCard.classList.contains('details')) {
                moreDetails.innerHTML = `<a class="moreDetails" href="#">Скрыть</a>`;
                commit.style.display = 'block';
                age_ter.style.display = 'block';
                dentist.style.display = 'block';
                cardiol.style.display = 'block';
            } else {
                moreDetails.innerHTML = `<a class="moreDetails" href="#">Показать больше</a>`;
                commit.style.display = 'none';
                age_ter.style.display = 'none';
                dentist.style.display = 'none';
                cardiol.style.display = 'none';
            }
        });
        medCard.append(moreDetails);
        medCard.append(commit);
        closeBtn.firstElementChild.addEventListener('click', function () {
            medCard.remove();
            List_Cards.delCard(medCard);
        });
        List_Cards.noItemsText();
    }

    static downloadVisit() {
        let request = fetch("http://cards.danit.com.ua/cards", {
            method: "GET",
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        console.log(token);
        request
            .then(message => message.json())
            .then(message => message.forEach(function (element) {
                console.log(element.id);
                List_Cards.renderCards(element);
            }));

        let noItems = document.querySelectorAll("medCard");
        if (noItems === undefined) {
            noItemsText.style.display = 'block';
            console.log('gecncsdjnvjsd;fbvf df')
        }
        List_Cards.noItemsText();
    }

    static delCard(medCard) {
        console.log(medCard.id);
        let request = fetch(`http://cards.danit.com.ua/cards/${medCard.id}`, {
            method: "DELETE",
            headers: {
                Authorization: `Bearer ${token}`
            }
        });
        request
            .then(message => message.json())
            .then(message => console.log(message))
        List_Cards.noItemsText();
    }

    static noItemsText() {
        // убирает надпись если есть хоть одна карта
        if (document.querySelectorAll('.medCard').length > 0) {
            document.querySelector('.noItemsText').classList.add('inactive');
        } else {
            document.querySelector('.noItemsText').classList.remove('inactive');
        }
    }
}

//////// Класс Терапевт
class Therapist extends Visit {
    constructor(modalContent) {
        super(modalContent);
    }

    static renderTherapist(modalContent) {
        modalContent.innerHTML = '';
        ///////////////Возраст
        let input_blocks_age = document.createElement('input');
        input_blocks_age.name = 'age';
        input_blocks_age.setAttribute('required', 'required');
        input_blocks_age.setAttribute('type', 'number');
        input_blocks_age.setAttribute('id', 'age');
        input_blocks_age.setAttribute('placeholder', 'Возраст');
        modalContent.append(input_blocks_age);
    }
}

//////// Класс Стоматолог
class Dentist extends Visit {
    constructor(modalContent) {
        super(modalContent);
    }

    static renderDentist(modalContent) {
        modalContent.innerHTML = '';
        ///////////////дата последнего посещения
        let input_date_of_last_visit = document.createElement('input_date_of_last_visit');
        input_date_of_last_visit.classList.add("input_date_of_last_visit");
        input_date_of_last_visit.innerHTML += `<input name = 'last_visit' type="date" required pattern="[0-9]{4}-[0-9]{2}-[0-9]{2}" id="VisitDate" placeholder="Дата посещения">`;
        modalContent.append(input_date_of_last_visit);
    }
}

//////// Класс Кардиолог
class Cardiologist extends Visit {
    constructor(modalContent) {
        super(modalContent);
    }

    static renderCardiologist(modalContent) {
        modalContent.innerHTML = '';
        ///////////////Давление
        let input_pressure = document.createElement('input');
        input_pressure.name = 'pressure';
        input_pressure.setAttribute('required', 'required');
        input_pressure.setAttribute('id', 'pressure');
        input_pressure.setAttribute('placeholder', 'Обычное давление');
        modalContent.append(input_pressure);
        ///////////////индекс массы тела
        let input_body_mass_index = document.createElement('input');
        input_body_mass_index.name = 'mass_index';
        input_body_mass_index.setAttribute('required', 'required');
        input_body_mass_index.setAttribute('id', 'mass_index');
        input_body_mass_index.setAttribute('placeholder', 'Индекс массы тела');
        modalContent.append(input_body_mass_index);
        ///////////////переносимые заболевания
        let input_tolerable_diseases = document.createElement('input');
        input_tolerable_diseases.name = 'tolerable_diseases';
        input_tolerable_diseases.setAttribute('required', 'required');
        input_tolerable_diseases.setAttribute('id', 'diseases');
        input_tolerable_diseases.setAttribute('placeholder', 'Переносимые заболевания');
        modalContent.append(input_tolerable_diseases);
        ///////////////Возраст пациента
        let input_blocks_age = document.createElement('input');
        input_blocks_age.name = 'age';
        input_blocks_age.setAttribute('required', 'required');
        input_blocks_age.setAttribute('type', 'number');
        input_blocks_age.setAttribute('id', 'age');
        input_blocks_age.setAttribute('placeholder', 'Возраст');
        modalContent.append(input_blocks_age);
    }
}

Visit.render();
